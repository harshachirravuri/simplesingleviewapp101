//
//  ViewController.m
//  SimpleSingleViewApp101
//
//  Created by Gaurav Gupta on 03/10/15.
//  Copyright © 2015 Yow2br. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnHelloWorl;
@property (weak, nonatomic) IBOutlet UILabel *lblHelloWorld;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  _lblHelloWorld.textColor = [UIColor redColor];
  
  // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)showHelloWorld:(id)sender {
  _lblHelloWorld.text = @"HEllo World";
  [_btnHelloWorl setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.4]];

}

@end
